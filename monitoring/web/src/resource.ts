import * as d3 from 'd3';

export class ResourceMonitor {
	constructor() {
		// Register interval timer to refresh the resource usage. 
		setInterval(() => {
			this.refreshResourceUsage();
		}, 1000);

		document.getElementById("profile-button")
			.addEventListener("click", () => {
				this.getProfilingResult();
			})
		document.getElementById("profile-button2")
			.addEventListener("click", () => {
				this.getProfilingResult2();
			})
	}

	refreshResourceUsage() {
		fetch('/api/resource').
			then(res => res.json()).
			then(data => {
				const cpuSpan = document.getElementById('cpu-usage');
				const memSpan = document.getElementById('mem-usage');

				cpuSpan.innerHTML = this.formatCPUPercent(data.cpu_percent);
				memSpan.innerHTML = this.formatBytes(data.memory_size);
			})
	}

	formatCPUPercent(percent: number) {
		return percent.toFixed(0) + '%';
	}

	formatBytes(bytes: number) {
		const sizes = ['B', 'KB', 'MB', 'GB', 'TB'];

		if (bytes == 0) return '0';

		const i = Math.floor(Math.log(bytes) / Math.log(1024));

		return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i];
	}

	getProfilingResult() {
		fetch('/api/profile').
			then(res => res.json())
			.then(data => {
				console.log(data);
				const network = this.pprofDataToNetwork(data);
				console.log(network);
				this.visualizePProfNetwork(network);
			})
	}

	getProfilingResult2() {
		fetch('/api/profile').
			then(res => res.json())
			.then(data => {
				console.log(data);
				const network = this.pprofDataToNetwork(data);
				console.log(network);
				this.visualizePProfNetwork2(network);
			})
	}

	pprofDataToNetwork(data: any): PProfNetwork {
		const network: PProfNetwork = new PProfNetwork();

		for (const func of data.Function) {
			const node: PProfNode = new PProfNode(func);
			network.nodes.push(node);
		}

		let totalTime = 0;
		for (const sample of data.Sample) {
			totalTime += sample.Value[0];

			for (let i = 0; i < sample.Location.length; i++) {
				const loc = sample.Location[i];
				const node = network.nodes[loc.Line[0].Function.ID - 1];

				node.time += sample.Value[0];
				if (i == 0) {
					node.selfTime += sample.Value[0];
				}
			}

			for (let i = 0; i < sample.Location.length - 1; i++) {
				const caller = sample.Location[i + 1].Line[0].Function.ID - 1;
				const callee = sample.Location[i].Line[0].Function.ID - 1;

				const edge = network.getOrCreateEdge(caller, callee);
				edge.time += sample.Value[0];
			}
		}

		for (const node of network.nodes) {
			node.selfTimePercentage = node.selfTime / totalTime;
			node.timePercentage = node.time / totalTime;
		}

		network.nodes = network.nodes.sort((a, b) => b.time - a.time);

		for (const edge of network.edges) {
			edge[1].timePercentage = edge[1].time / totalTime;
		}

		for (let i = 0; i < network.nodes.length; i++) {
			const node = network.nodes[i];
			node.index = i;
		}
		return network;
	}

	visualizePProfNetwork(network: PProfNetwork) {

		//convert network to json
		var myjson = {"children": []};
		var namelist = [];
		for (var i=0; i<network.nodes.length; i++) {
			var valueDict = {};
			var funcName = network.nodes[i].func["Name"];
			var funcNameParts = funcName.split('/');
			var groupName = funcNameParts[funcNameParts.length - 1];
			if (!namelist.includes(groupName.split('.')[0])) {
				valueDict["Name"] = groupName.split('.')[0];
				valueDict["children"] = [];
				namelist.push(groupName.split('.')[0])
				myjson['children'].push(valueDict);
			}
		}
		for (var i=0; i<network.nodes.length; i++) {
			var valueDict = {};
			var funcName = network.nodes[i].func["Name"];
			var funcNameParts = funcName.split('/');
			var groupName = funcNameParts[funcNameParts.length - 1].split('.')[0];
			valueDict["Name"] = funcNameParts[funcNameParts.length - 1];
		 	valueDict["TimePercentage"] = network.nodes[i].timePercentage;
			valueDict["SelfTimePercentage"] = network.nodes[i].selfTimePercentage;
			for (const group of myjson.children){
				if (group.Name === groupName){
					group.children.push(valueDict);
				}
			}
		}
		console.log(myjson);
		
		// visualization portion
		const rightPane = document.getElementById('right-pane');
		rightPane.innerHTML = '<div id="pprof-tooltip"><div>';
		var margin = {top: 10, right: 10, bottom: 10, left: 10},
		width = 2100 - margin.left - margin.right,
		height = 2700 - margin.top - margin.bottom;
		const svg = document.createElementNS(
			'http://www.w3.org/2000/svg', 'svg');
		rightPane.appendChild(svg);
		const d3SVG = d3.select(svg);
		d3SVG.attr('width', '100%');
		d3SVG.attr('height', '100%');
		d3SVG.attr('overflow', 'scroll');

		const nodeGroup = d3SVG.append('g').attr('id', 'pprof-nodes');

		d3SVG.call(d3.zoom().on('zoom', (e) => {
			nodeGroup.attr('transform', e.transform);
		}));

		var tooltip = d3.select("body")
			.append("div")
			.attr("class", "tooltip")
			.style("opacity", 0)
			.style("background-color", "white")
			.style("border", "1px solid black")
			.style("padding", "10px")
			.style("font-size", "12px");
		var root = d3.hierarchy(myjson).sum(function(d){ return d.SelfTimePercentage + 0.001})

		// Then d3.treemap computes the position of each element of the hierarchy
		d3.treemap()
		.size([width, height])
		.paddingTop(16)
		.paddingRight(7)
		.paddingInner(3)
		(root)
	
		// Add's rectangles
		var cell = nodeGroup
		.selectAll("rect")
		.data(root.leaves())
		.enter().append("g")
		.attr('transform', function (d) { return `translate(${d.x0},${d.y0})`; });

		const customColorRange = [
			"#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2",
			"#7f7f7f", "#bcbd22", "#17becf", "#aec7e8", "#ffbb78", "#98df8a", "#ff9896",
			"#c5b0d5", "#c49c94", "#f7b6d2", "#c7c7c7", "#dbdb8d", "#9edae5", "#393b79",
			"#637939", "#8ca252", "#b5cf6b", "#cedb9c", "#bd9e39", "#e7ba52", "#e7cb94",
			"#843c39", "#ad494a", "#d6616b", "#e7969c", "#7b4173", "#a55194", "#ce6dbd",
			"#de9ed6", "#3182bd", "#6baed6", "#9ecae1", "#c6dbef", "#e6550d", "#fd8d3c",
			"#fdae6b", "#fdd0a2", "#31a354", "#74c476", "#a1d99b", "#c7e9c0", "#756bb1",
			"#9e9ac8", "#bcbddc", "#dadaeb"
		];

		// Create a color scale for each group of rectangles
		const parents = root.descendants().filter(d => d.depth === 1);
		const colorScale = d3.scaleOrdinal()
			.domain(parents.map((_, i) => i))
			.range(customColorRange);
		// Minimum rectangle size
		const minRectSize = 5;

		// Create the rectangles
		cell.append("rect")
			.attr('width', function (d) { return Math.max(d.x1 - d.x0, minRectSize); }) 
			.attr('height', function (d) { return Math.max(d.y1 - d.y0, minRectSize); }) 
			.style("fill", d => colorScale(parents.indexOf(d.parent)))
			.on("mouseover", (event, d) => {
				tooltip.style("opacity", .9);
				tooltip.html(`
				<div>${d.data.Name}</div>
				<div>Time: ${d.data.TimePercentage * 100}%</div>
				<div>Self Time: ${d.data.SelfTimePercentage * 100}%</div>
			`)
					.style("left", (event.pageX + 15) + "px")
					.style("top", (event.pageY + 15)+"px");
			})
			.on("mousemove", (event, d) => {
				tooltip.style("left", (event.pageX + 15) + "px")
					.style("top", (event.pageY + 15) + "px");
			})
			.on("mouseout", function(d) {
				tooltip.style("opacity", 0)
				.html("") 
				.style("left", "-9999px") 
				.style("top", "-9999px");
			})
			
	
		function checkTextWidth(text, maxWidth) {
			if (text.node().getComputedTextLength() > maxWidth) {
				text.text(null);
			}
		}

		function showBorderTooltip(event, d) {
			tooltip.style("opacity", .9);
			tooltip.html(`
				<div>${d.data.Name}</div>
			`)
				.style("left", (event.pageX + 15) + "px")
				.style("top", (event.pageY + 15) + "px");
		}
		function hideBorderTooltip() {
			tooltip.style("opacity", 0)
			.html("") 
				.style("left", "-9999px") 
				.style("top", "-9999px");
		}

		// Create the groupings for the rectangles
		nodeGroup.selectAll('.parent')
		.data(root.descendants().filter(d => d.depth === 1))
		.enter().append('rect')
		.attr('class', 'parent')
		.attr('x', d => d.x0)
		.attr('y', d => d.y0)
		.attr('width', d => d.x1 - d.x0)
		.attr('height', d => d.y1 - d.y0)
		.style('stroke', 'black')
		.style('fill', 'none')

		nodeGroup.selectAll('.padding-overlay')
		.data(root.descendants().filter(d => d.depth === 1))
		.enter().append('rect')
		.attr('class', 'padding-overlay')
		.attr('x', d => d.x0)
		.attr('y', d => d.y0 + 4)
		.attr('width', d => d.x1 - d.x0)
		.attr('height', d => 12) 
		.style('fill', 'none')
		.style('pointer-events', 'all')
		.on("mouseover", (event, d) => {
			showBorderTooltip(event, d);
		})
		.on("mousemove", (event, d) => {
			tooltip.style("left", (event.pageX + 15) + "px")
					.style("top", (event.pageY + 15) + "px");
		})
		.on("mouseout", hideBorderTooltip);
	
		// Display the name of the parent on the border
		nodeGroup.selectAll('.parent-label')
			.data(root.descendants().filter(d => d.depth === 1))
			.enter().append('text')
			.attr('class', 'parent-label')
			.attr('x', d => d.x0 + 5)
			.attr('y', d => d.y0 + 13)
			.text(d => d.data.Name)
			.style('font-size', '14px')
			.style('font-weight', 'bold')
			.each(function (d) {
				const text = d3.select(this);
				text.text(d.data.Name);
				const maxWidth = d.x1 - d.x0 - 10;
				checkTextWidth(text, maxWidth);
			});
		
		// add text to each rectangle
		cell.append('text')
		.attr("dy", "1em")
		.attr("dx", "0.5em")
		.text(function (d) { return d.data.Name; })
		.each(function(d) {
			var text = d3.select(this),
				width = d.x1 - d.x0 - parseFloat(text.attr("dx")),
				height = d.y1 - d.y0 - parseFloat(text.attr("dy")),
				content = text.text();
			if (text.node().getComputedTextLength() > width ||
				text.node().getBBox().height > height) {
				text.text(null);
			}
		})
		.style("font-size", "12px")
        .style("fill", "white");
		
	}

	// Original visualization
	visualizePProfNetwork2(network: PProfNetwork) {
		const rightPane = document.getElementById('right-pane');
		rightPane.innerHTML = '<div id="pprof-tooltip"><div>';

		const svg = document.createElementNS(
			'http://www.w3.org/2000/svg', 'svg');
		rightPane.appendChild(svg);

		const d3SVG = d3.select(svg);
		d3SVG.attr('width', '100%');
		d3SVG.attr('height', '100%');
		d3SVG.attr('overflow', 'scroll');

		d3SVG.append("defs").append("marker")
			.attr("id", "arrowhead")
			.attr("viewBox", "0 -66 200 200")
			.attr("refX", 5)
			.attr("refY", 2)
			.attr("markerWidth", 18)
			.attr("markerHeight", 12)
			.attr("orient", "auto")
			.attr("markerUnits", "userSpaceOnUse")
			.append("path")
			.attr("d", "M0,-66 L0,66 L200,0");


		const nodeGroup = d3SVG.append('g').attr('id', 'pprof-nodes');
		const edgeGroup = d3SVG.append('g').attr('id', 'pprof-edges');
		d3SVG.call(d3.zoom().on('zoom', (e) => {
			nodeGroup.attr('transform', e.transform);
			edgeGroup.attr('transform', e.transform);
		}));

		const colorScale = d3.scaleSequential(
			d3.interpolateOranges).domain([0, 1]);


		// Create the nodes.
		const node = nodeGroup.selectAll('rect')
			.data(network.nodes, (d: PProfNode) => d.func.ID)
			.enter().append('g')
			.attr('class', 'pprof-node')
			.attr('transform', (d: PProfNode, i) => {
				return `translate(0, ${i * 30})`;
			});

		node.append('rect')
			.attr('width', '15')
			.attr('height', '15')
			.attr('fill', (d: PProfNode) => {
				return colorScale(d.timePercentage);
			})
			.attr('stroke', '#000')
			.attr('stroke-width', '1px')
			.attr('rx', '5px')
			.attr('ry', '5px')
			.attr('x', '2')
			.attr('y', '2')
			.attr('class', 'pprof-node-rect')
			.on("mouseover", (e: MouseEvent, d: PProfNode) => {
				this.showTooltip(e, d, network)
			}).on("mouseout", (e: MouseEvent, d: PProfNode) => {
				this.hideTooltip(e, d)
			});

		node.append('rect')
			.attr('width', '15')
			.attr('height', '15')
			.attr('fill', (d: PProfNode) => {
				return colorScale(d.selfTimePercentage);
			})
			.attr('stroke', '#000')
			.attr('stroke-width', '1px')
			.attr('rx', '5px')
			.attr('ry', '5px')
			.attr('x', '20')
			.attr('y', '2')
			.attr('class', 'pprof-node-rect')
			.on("mouseover", (e: MouseEvent, d: PProfNode) => {
				this.showTooltip(e, d, network)
			}).on("mouseout", (e: MouseEvent, d: PProfNode) => {
				this.hideTooltip(e, d)
			});

		node.append('text')
			.attr('x', '45')
			.attr('y', '15')
			.attr('class', 'pprof-node-text')
			.text((d: PProfNode) => {
				const funcName = d.func['Name'];
				const funcNameParts = funcName.split('/');
				return funcNameParts[funcNameParts.length - 1];
			})

		// Add edges
		edgeGroup.selectAll('path')
			.data(network.edges.values())
			.enter().append('path')
			.attr('d', (d: PProfEdge) => {
				const x1 = 0;
				const y1 = d.caller.index * 30 + 7;
				const x2 = -1;
				const y2 = d.callee.index * 30 + 7;

				const r = Math.abs(y2 - y1) / 2;
				// Return a path that represent a semi-circle between the two nodes.

				let direction = '0';
				// if (y2 < y1) {
				// 	direction = '1';
				// }

				return `M ${x1} ${y1} A ${r} ${r} 0 0 ${direction} ${x2} ${y2}`;
			}).attr('stroke', (d: PProfEdge) => {
				// return colorScale(d.timePercentage);
				return '#999999';
			}).attr('stroke-width', (d: PProfEdge) => {
				return d.timePercentage * 10;
			}).attr('fill', 'none')
			.attr("marker-end", "url(#arrowhead)")
			.attr('marker-end-size', '6');
	}

	showTooltip(e: MouseEvent, d: PProfNode, network: PProfNetwork) {
		const tooltip = document.getElementById('pprof-tooltip');
		tooltip.innerHTML = `
			<div>${d.func['Name']}</div>
			<div>Time: ${d.timePercentage * 100}%</div>
			<div>Self Time: ${d.selfTimePercentage * 100}%</div>
		`;
		tooltip.style.display = 'block';
		tooltip.style.left = `${e.pageX}px`;
		tooltip.style.top = `${e.pageY}px`;

		const edgeGroup = d3.select('#pprof-edges');
		edgeGroup.selectAll('path')
			.data(network.edges.values())
			.filter((e: PProfEdge) => {
				return e.caller.index !== d.index && e.callee.index !== d.index;
			}).attr('opacity', '.3');
	}

	hideTooltip(e: MouseEvent, d: PProfNode) {
		const tooltip = document.getElementById('pprof-tooltip');
		tooltip.style.display = 'none';

		const edgeGroup = d3.select('#pprof-edges');
		edgeGroup.selectAll('path').attr('opacity', '1');
	}


}



class PProfNetwork {
	nodes: Array<PProfNode>;
	edges: Map<string, PProfEdge>;

	constructor() {
		this.nodes = [];
		this.edges = new Map<string, PProfEdge>();
	}

	static getEdgeName(callerIndex: number, calleeIndex: number) {
		return `${callerIndex}-${calleeIndex}`;
	}

	getOrCreateEdge(callerIndex: number, calleeIndex: number) {
		const edgeName = PProfNetwork.getEdgeName(callerIndex, calleeIndex);
		let edge = this.edges.get(edgeName);

		if (edge == null) {
			edge = new PProfEdge(
				this.nodes[callerIndex],
				this.nodes[calleeIndex]
			);
			this.edges.set(edgeName, edge);

			this.nodes[callerIndex].addEdge(edge);
		}

		return edge;
	}
}

class PProfNode {
	index: number;
	func: any;
	selfTime: number;
	time: number
	edges: Array<PProfEdge>;
	timePercentage: number;
	selfTimePercentage: number;

	constructor(func: any) {
		this.func = func;
		this.selfTime = 0;
		this.time = 0;
		this.edges = [];
	}

	addEdge(edge: PProfEdge) {
		this.edges.push(edge);
	}
}

class PProfEdge {
	time: number;
	timePercentage: number;
	caller: PProfNode;
	callee: PProfNode;

	constructor(caller: PProfNode, callee: PProfNode) {
		this.time = 0;
		this.caller = caller;
		this.callee = callee;
	}
}

